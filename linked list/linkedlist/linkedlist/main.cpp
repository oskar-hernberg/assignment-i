// linkedlist.cpp : Defines the entry point for the console application.
// Hi, launch in visual studio with ctrl+f5, the left column of numbers is the elements value, the right column is that elements next neighbour!

#include "stdafx.h"
#include "main.h"
#include "ListManager.h"
#include "element.h"
#include "unit.hpp"

#define _CRTDBG_MAP_ALLOC
#include <stdlib.h>
#include <crtdbg.h>

int _tmain(int argc, _TCHAR* argv[])
{
	ListManager<int> list;
	
	int numbers[10] = { 11, 242, 2, 12, 23, 24, 654, 45, 23, 10};
	list.populatelist(numbers);
	verify(10, list.GetLastElement()->GetValue(), "Get the last element in the list");
	
	list.push_front(79);
	verify(79, list.GetFirstElement()->GetValue(), "get the first element in the list");

	list.pop_front();
	verify(11, list.GetFirstElement()->GetValue(), "11 should be first after the previous first element got removed");

	list.push_back(234);
	verify(234, list.GetLastElement()->GetValue(), "234 should be last after push back");
	
	list.pop_back();
	verify(10, list.GetLastElement()->GetValue(), "10 should be last after pop back");

	list.push_back(list.at(2)->GetValue());
	verify(2, list.GetLastElement()->GetValue(), "2 should be last after pushback list.at()");

	list.push_back(list.find_value(654));
	verify(654, list.GetLastElement()->GetValue(), "654 should be last after finding value 654");

	list.remove(654);
	list.remove(654);
	verify(2, list.GetLastElement()->GetValue(), "2 should now be the last element in the list");


	//uncomment if you want to try list.clear()!!!!!!
	//list.clear();
	//verify(0, list.GetLength(), "list should be empty");

	list.insert_after(2, 60);
	verify(60, list.at(3)->GetValue(), "value should be 60 at position 3");

	

	cout << "The list has " << list.GetLength() << " elements in it" << endl;

	//Fail Check
	if (!list.GetFirstElement())
	{
		cout << "The list is empty" << endl;
		return 0;
	}




	bool loop = false;
	Element<int> loopelement;
	if (list.GetFirstElement())
	{
		loopelement = *list.GetFirstElement();
	}

	while (loop == false)
	{
		
		if (loopelement.GetNextElement() != nullptr)
		{
			cout << loopelement.GetValue() << " " << loopelement.GetNextElement()->GetValue() << endl;
			loopelement = *loopelement.GetNextElement();
		}
		else
		{
			cout << loopelement.GetValue() << endl;
			loop = true;
		}
		
	}



	Element<int>* elementd = nullptr;
	bool d = false;
	while (d == false)
	{
		
		elementd = list.GetFirstElement();

		if (list.GetFirstElement()->GetNextElement() != nullptr)
		{
			list.SetFirst(list.GetFirstElement()->GetNextElement());
		}
		else
		{
			d = true;
		}
		
		delete elementd;
		elementd = NULL;
		

	}

	
	_CrtDumpMemoryLeaks();
	return 0;
}

