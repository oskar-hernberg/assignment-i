#pragma once


template <class Tvalue>
class Element
{
public:
	Element() { m_value = 0; m_nextelement = nullptr; m_active = true; }
	
	void SetValue(Tvalue value) { m_value = value;}
	
	void SetNextElement(Element* element) { m_nextelement = element;}
	void SetActive(bool b) { m_active = b; }

	Tvalue GetValue() { return m_value;}
	
	Element* GetNextElement()
	{ 
		return m_nextelement;
	}
	bool GetActive() { return m_active; }

private:
	
	int m_value;
	Element* m_nextelement;

	bool m_active;
};