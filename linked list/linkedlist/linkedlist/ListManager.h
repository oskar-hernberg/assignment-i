#pragma once

#include "element.h"


template <class Tvalue>

class ListManager
{
public:
	ListManager() { m_firstelement = nullptr; m_lastelement = nullptr; m_activeelement = nullptr; m_length = 0; }

	void SetFirst(Element<int>* first) 
	{ 
		m_firstelement = first;
	}
	void SetLast( Element<int>* last) 
	{ 
		m_lastelement = last;
	}

	void populatelist(int array[])
	{
		Element<int>* lastelement = nullptr;

		for (int i = 0; i < 10; i++)
		{
			Element<int>* element = new Element < int > ;
			m_length += 1;
			element->SetValue(array[i]);
			
			if (i != 0)
			{
				lastelement->SetNextElement(element);
			}
			if (i == 0)
			{
				SetFirst(element);
			}
			if (i == 9)
			{
				SetLast(element);
			}
			lastelement = element;
		}
	}


	void push_front(Tvalue value)
	{
		Element<int>*  element = new Element < int > ;
		m_length += 1;
		element->SetValue(value);
		element->SetNextElement(m_firstelement);
		m_firstelement = element;
		if (!m_lastelement)
		{
			m_lastelement = element;
		}
		

	}

	void push_front(Element<int>* value)
	{
		if (value)
		{
			Element<int>*  element = new Element < int > ;
			m_length += 1;
			element->SetValue(value->GetValue());
			element->SetNextElement(m_firstelement);
			m_firstelement = element;
			if (!m_lastelement)
			{
				m_lastelement = element;
			}
		}
	}
	void pop_front() 
	{
		Element<int>* backup = nullptr;
		backup = m_firstelement;
		m_firstelement = m_firstelement->GetNextElement();
		delete backup;
		backup = nullptr;
		m_length -= 1;

	}

	void push_back(Tvalue value)
	{
		Element<int>*  element = new Element < int > ;
		m_length += 1;
		element->SetValue(value);
		if (!m_lastelement)
		{
			push_front(value);
		}
		else
		{
			m_lastelement->SetNextElement(element);
			m_lastelement = element;
		}
	}

	void push_back(Element<int>* value)
	{
		if (value)
		{
			Element<int>*  element = new Element < int > ;
			m_length += 1;
			element->SetValue(value->GetValue());
			if (!m_lastelement)
			{
				push_front(value->GetValue());
			}
			else
			{
				m_lastelement->SetNextElement(element);
				m_lastelement = element;
			}
		}
	}

	Element<int>* at(int position)
	{
		Element<int>*  element = nullptr;
		element = m_firstelement;
		if (position < m_length )
		{
			for (int i = 0; i <= position; i++)
			{
				if (i == position)
				{
					return element;
				}
				else
				{
					element = element->GetNextElement();
				}
			}
		}
		return nullptr;
	}

	void pop_back()
	{
		Element<int>*  element = nullptr;
		Element<int>*  backup = nullptr;
		backup = m_lastelement;
		m_length -= 1;
		element = m_firstelement;
		bool d = false;
		while (d == false)
		{
			if (element->GetNextElement()->GetNextElement() != nullptr)
			{
				element = element->GetNextElement();
			}
			else
			{
				element->SetNextElement(NULL);
				m_lastelement = element;
				delete backup;
				backup = nullptr;
				d = true;
			}
		}
	}

	Element<int>* find_value(Tvalue value)
	{
		Element<int>*  element = nullptr;
		element = m_firstelement;
		bool d = false;
		while (d == false)
		{
			if (element->GetValue() != value)
			{
				element = element->GetNextElement();
			}
			else if (element->GetValue() == value)
			{
				return element;
			}
			
			if (element->GetNextElement() == nullptr)
			{
				d = true;
			}
		}
		return nullptr;
	}

	void remove(Tvalue value)
	{
		Element<int>*  element = nullptr;
		Element<int>* backup = nullptr;
		element = m_firstelement;
		bool d = false;
		while (d == false)
		{
			if (element->GetValue() != value)
			{
					if (element->GetNextElement()->GetValue() != value)
					{
						element = element->GetNextElement();
					}					
			}
			if (element->GetNextElement() != nullptr)
			{
				if (element->GetValue() == value)
				{
					m_firstelement = element->GetNextElement();
					m_length -= 1;
					d = true;
				}
				else if (element->GetNextElement()->GetValue() == value)
				{
					
					backup = element->GetNextElement();
					if (element->GetNextElement()->GetNextElement() == nullptr)
					{
						m_lastelement = element;
					}
					element->SetNextElement(element->GetNextElement()->GetNextElement());
					d = true;
					m_length -= 1;
					delete backup;
					backup = nullptr;
				}
			}
			if (element->GetNextElement() == nullptr)
			{
				d = true;
			}
		}
		
	}

	void insert_after(Tvalue position, Tvalue value)
	{
		Element<int>*  element = new Element < int >;
		element->SetValue(value);
		element->SetNextElement(at(position)->GetNextElement());
		at(position)->SetNextElement(element);
	}

	void clear()
	{
		m_length = 0;
		m_firstelement = nullptr;
		m_lastelement = nullptr;
	}

	Element<int>* GetFirstElement() { return m_firstelement; }
	Element<int>* GetLastElement() { return m_lastelement; }
	int GetLength() { return m_length; }
private:
	Element<int> *m_firstelement;
	Element<int> *m_lastelement;
	Element<int> *m_activeelement;
	int m_length;

};