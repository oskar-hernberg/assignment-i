// binary.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "element.h"
#include "listmanager.h"
#include "unit.h"

int _tmain(int argc, _TCHAR* argv[])
{
	ListManager<int> tree;
	Element<int>* null = nullptr;
	tree.Insert(8);
	verify(8, tree.GetFirst()->GetValue(), "first element should be 8");
	tree.Insert(5);
	verify(5, tree.GetFirst()->GetLeftElement()->GetValue(), "the left element of the first element should be 4");
	tree.Insert(12);
	//verify(12, tree.GetFirst()->GetRightElement()->GetValue(), "the right element of the first element should be 12");

	tree.Insert(2);
	tree.Insert(6);
	tree.Insert(14);
	tree.Insert(11);
	tree.Insert(13);
	verify(2, tree.GetFirst()->GetLeftElement()->GetLeftElement()->GetValue(), "the value should be 2");
	verify(6, tree.GetFirst()->GetLeftElement()->GetRightElement()->GetValue(), "the value should be 6");
	verify(11, tree.GetFirst()->GetRightElement()->GetLeftElement()->GetValue(), "the value should be 11");
	verify(14, tree.GetFirst()->GetRightElement()->GetRightElement()->GetValue(), "the value should be 14");
	

	verify(11, tree.Search(11)->GetValue(), "after searching for 11 it should return a element pointer with value 11");
	tree.remove(14);
	verify(13, tree.GetFirst()->GetRightElement()->GetRightElement()->GetValue(), "left element should be deleted");
	tree.remove(8);
	verify(5, tree.GetFirst()->GetValue(), "5 should be the first element in the tree");
	tree.inOrder(tree.GetFirst());
	cout << "\n" << endl;
	tree.preOrder(tree.GetFirst());
	cout << "\n" << endl;
	tree.postOrder(tree.GetFirst());
	cout << "\n" << endl;
	verify(6, tree.size(), "size of binary tree should be 6");
	tree.clear(tree.GetFirst());
	//_CrtDumpMemoryLeaks();
	return 0;
}

