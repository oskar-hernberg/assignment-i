#pragma once

template <class Tvalue>
class Element
{
public:
	Element() { m_value = 0; m_leftelement = nullptr; m_rightelement = nullptr; m_previouselement = nullptr; }

	void SetValue(Tvalue value) { m_value = value; }

	void SetLeftElement(Element* element) { m_leftelement = element; }
	void SetRightElement(Element* element) { m_rightelement = element; }
	void SetPreviousElement(Element* element) { m_previouselement = element; }
	
	Tvalue GetValue() {
		if (this)
		{
			return m_value;
		}
		return 88888888888;
	}

	Element* GetLeftElement()
	{
		return m_leftelement;
	}
	Element* GetRightElement()
	{
		return m_rightelement;
	}
	Element* GetPreviousElement()
	{
		return m_previouselement;
	}
	

private:

	int m_value;
	Element* m_leftelement;
	Element* m_rightelement;
	Element* m_previouselement;
	
};