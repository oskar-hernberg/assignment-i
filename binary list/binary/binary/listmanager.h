#pragma once

#include "element.h"
#include <cmath>

template <class Tvalue>

class ListManager
{
public:
	ListManager() { m_firstelement = nullptr; m_length = 0; }

	void SetFirst(Element<int>* first)
	{
		m_firstelement = first;
	}

	Element<int>* GetFirst()
	{
		return m_firstelement;
	}
	void Insert(Tvalue value)
	{
		Element<int>* element = new Element < int > ;
		element->SetValue(value);
		Element<int>* previous = nullptr;
		previous = m_firstelement;
		bool d = false;
		while (d == false)
		{
			if (m_length == 0)
			{
				element->SetValue(value);
				m_firstelement = element;
				m_length += 1;
				d = true;
				break;
			}
			else
			{
				if (previous->GetValue() > value)
				{
					if (previous->GetLeftElement() == nullptr)
					{
						previous->SetLeftElement(element);
						element->SetPreviousElement(previous);
						d = true;
						m_length += 1;
						break;
					}
					else
					{
						previous = previous->GetLeftElement();
					}
				}
				else if (previous->GetValue() <= value)
				{
					if (previous->GetRightElement() == nullptr)
					{
						previous->SetRightElement(element);
						element->SetPreviousElement(previous);
						d = true;
						m_length += 1;
						break;
					}
					else
					{
						previous = previous->GetRightElement();
					}
				}
			}
		}
	}

	Element<int>* Search(Tvalue value)
	{
		bool d = false;
		Element<int>* previous = nullptr;
		previous = m_firstelement;
		while (d == false)
		{
			if (previous->GetValue() == value)
			{
				return previous;
			}
			else if (previous->GetLeftElement() == nullptr && previous->GetRightElement() == nullptr)
			{
				return 0;
			}
			else
			{
				if (previous->GetValue() > value)
				{
					previous = previous->GetLeftElement();
				}
				else if (previous->GetValue() < value)
				{
					previous = previous->GetRightElement();
				}
			}
		}
		return 0;
	}

	void remove(Tvalue value)
	{
		bool d = false;
		Element<int>* previous = nullptr;
		previous = Search(value);
		
		if (previous)
		{
			while (d == false)
			{

				if (previous->GetLeftElement() != nullptr && previous->GetRightElement() != nullptr)
				{
					switch (abs(previous->GetValue() - previous->GetLeftElement()->GetValue()) >= abs(previous->GetValue() - previous->GetRightElement()->GetValue())) //Checks which value is closest
					{
					case true:
						previous->SetValue(previous->GetRightElement()->GetValue());
						previous = previous->GetRightElement();
					
						break;
					case false:
						previous->SetValue(previous->GetLeftElement()->GetValue());
						previous = previous->GetLeftElement();
						break;
					}
				}
				else if (previous->GetLeftElement() == nullptr && previous->GetRightElement() == nullptr)
				{
					if (previous != m_firstelement)
					{
						if (previous->GetValue() >= previous->GetPreviousElement()->GetValue())
						{
							previous->GetPreviousElement()->SetRightElement(nullptr);
						}
						else if (previous->GetValue() < previous->GetPreviousElement()->GetValue())
						{
							previous->GetPreviousElement()->SetLeftElement(nullptr);
						}
					}
					delete previous;
					previous = nullptr;
					d = true;
					m_length -= 1;
					break;
				}
				else
				{
					bool bigger = false;
					bool alone = false;
					if (previous->GetPreviousElement() == nullptr)
					{
						alone = true;
					}
					if (alone != true)
					{
						if (previous->GetValue() >= previous->GetPreviousElement()->GetValue())
						{
							bigger = true;
						}
					}
					
					if (previous->GetLeftElement() != nullptr)
					{
						if (alone == true)
						{
							m_firstelement = previous->GetLeftElement();
							previous->GetLeftElement()->SetPreviousElement(nullptr);
						}
						else {
							previous->GetLeftElement()->SetPreviousElement(previous->GetPreviousElement());
							switch (bigger)
							{
							case true:
								previous->GetPreviousElement()->SetRightElement(previous->GetLeftElement());
								break;
							case false:
								previous->GetPreviousElement()->SetLeftElement(previous->GetLeftElement());
								break;
							}
							
						}
						delete previous;
						previous = nullptr;
						d = true;
						m_length -= 1;
						break;
					}
					else if (previous->GetRightElement() != nullptr)
					{
						if (alone == true)
						{
							m_firstelement = previous->GetRightElement();
							previous->GetRightElement()->SetPreviousElement(nullptr);
						}
						else {
							previous->GetRightElement()->SetPreviousElement(previous->GetPreviousElement());
							switch (bigger)
							{
							case true:
								previous->GetPreviousElement()->SetRightElement(previous->GetRightElement());
								break;
							case false:
								previous->GetPreviousElement()->SetLeftElement(previous->GetRightElement());
								break;
							}
						}
						delete previous;
						previous = nullptr;
						d = true;
						m_length -= 1;
						break;

					}
				}
				
			}
		

		}
	}

	void clear(Element<int>* root)
	{
		if (root != NULL)
		{
			clear(root->GetLeftElement());
			clear(root->GetRightElement());
			delete root;
		}
	}

	void inOrder(Element<int>* n)
	{
		if (n)
		{
			inOrder(n->GetLeftElement());
			cout << n->GetValue() << " ";
			inOrder(n->GetRightElement());
		}
	}
	void preOrder(Element<int>* n) {
		if (n) {
			cout << n->GetValue() << " ";
			preOrder(n->GetLeftElement());
			preOrder(n->GetRightElement());
		}
	}
	void postOrder(Element<int>* n) {
		if (n) {
			postOrder(n->GetLeftElement());
			postOrder(n->GetRightElement());
			cout << n->GetValue() << " ";
		}
	}
	int size()
	{
		return m_length;
	}
	
private:
	Element<int> *m_firstelement;
	int m_length;
};